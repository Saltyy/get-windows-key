Option Explicit
Dim objshell, path, DigitalID, Result, scriptPath
Set objshell = CreateObject("WScript.Shell")

' Get the script's directory
scriptPath = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)

' Set registry key path
Path = "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\"

' Registry key value
DigitalID = objshell.RegRead(Path & "DigitalProductID")
Dim WinVersion, WinID, WinKey, WinData

' Get WinVersion, WinID, WinKey
WinVersion = "WinVersion: " & objshell.RegRead(Path & "ProductName")
WinID = "WinID: " & objshell.RegRead(Path & "ProductID")
WinKey = "WinKey: " & ConvertToKey(DigitalID)
WinData = "Windows Information:" & vbNewLine & WinVersion & vbNewLine & WinID & vbNewLine & WinKey

' Show message box and save to a file if confirmed
If vbYes = MsgBox(WinData & vbCrLf & vbCrLf & "Save to a file?", vbYesNo + vbQuestion, "BackUp Windows Key Information") Then
    Save WinData, scriptPath
End If

' Convert binary to chars
Function ConvertToKey(Key)
    Const KeyOffset = 52
    Dim isWin8, Maps, i, k, Current, KeyOutput, Last, keypart1, insert

    ' Check if OS is Windows 8
    isWin8 = (Key(66) \ 6) And 1
    Key(66) = (Key(66) And &HF7) Or ((isWin8 And 2) * 4)
    i = 24
    Maps = "BCDFGHJKMPQRTVWXY2346789"
    Do
        Current = 0
        k = 14
        Do
            Current = Current * 256
            Current = Key(k + KeyOffset) + Current
            Key(k + KeyOffset) = (Current \ 24)
            Current = Current Mod 24
            k = k - 1
        Loop While k >= 0
        i = i - 1
        KeyOutput = Mid(Maps, Current + 1, 1) & KeyOutput
        Last = Current
    Loop While i >= 0
    
    If isWin8 = 1 Then
        keypart1 = Mid(KeyOutput, 2, Last)
        insert = "N"
        KeyOutput = Replace(KeyOutput, keypart1, keypart1 & insert, 2, 1, 0)
        If Last = 0 Then KeyOutput = insert & KeyOutput
    End If
    
    ConvertToKey = Mid(KeyOutput, 1, 5) & "-" & Mid(KeyOutput, 6, 5) & "-" & Mid(KeyOutput, 11, 5) & "-" & Mid(KeyOutput, 16, 5) & "-" & Mid(KeyOutput, 21, 5)
End Function

' Save data to a file
Function Save(Data, PathToSave)
    Dim fso, txt
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    ' Save data into text file in the script's directory
    Set txt = fso.CreateTextFile(PathToSave & "\WindowsInformations.txt")
    txt.WriteLine Data
    txt.Close
End Function
