# Get Windows Key
get your current Windows Version, Product ID and Key for safety.


## How to use:

1. `clone & execute`

    Clone the project and double click the ".getWindowsKey.vbs" file.

2. `Popup`

    Now there should be a popup with your Informations. If you want to save them into a file, press "Yes" else "No".



> **Note**: The File should be saved with the name "WindowsInformations.txt", in the same path as the executed script.